"""django_init URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from django.conf.urls import include, url
from aplicaciones.principal.views import inicio, crearMenu, editarMenu, eliminarMenu, Search
from aplicaciones.principal.class_view import MenuList, MenuCreate, MenuUpdate, MenuDelete, PositionsList, PositionsCreate, PositionsUpdate, PositionsDelete, FinalMenu

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', MenuList.as_view(), name='index'),
    path('crear_menu/', MenuCreate.as_view(), name='crear_menu'),
    path('editar_menu/<int:pk>', MenuUpdate.as_view(), name='editar_menu'),
    path('eliminar_menu/<int:pk>', MenuDelete.as_view(), name='eliminar_menu'),
    path('positions/', PositionsList.as_view(), name='index_position'),
    path('crear_position/', PositionsCreate.as_view(), name='crear_position'),
    path('editar_position/<int:pk>', PositionsUpdate.as_view(), name='editar_position'),
    path('eliminar_position/<int:pk>', PositionsDelete.as_view(), name='eliminar_position'),
    path('resultado_menu/', FinalMenu.as_view(), name='resultado_menu'),
    path('search/', Search, name='search'),
]

