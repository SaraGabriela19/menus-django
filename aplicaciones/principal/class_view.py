from django.shortcuts import redirect, render
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.urls import reverse_lazy
from .models import Menu, Positions
from .forms import menuForm, positionsForm

class MenuList(ListView):
    model = Menu
    template_name = 'index.html'

    def get_queryset(self):
        return self.model.objects.all()

class MenuCreate(CreateView):
    model = Menu
    form_class = menuForm
    template_name = 'crear_menu.html'
    success_url = reverse_lazy('index')

class MenuUpdate(UpdateView):
    model = Menu
    form_class = menuForm
    template_name = 'crear_menu.html'
    success_url = reverse_lazy('index')   

class MenuDelete(DeleteView):
    model = Menu
    template_name = 'verificacion.html'
    success_url = reverse_lazy('index')     

class PositionsList(ListView):
    model = Positions
    template_name = 'index_position.html'

    def get_queryset(self):
        return self.model.objects.all()

class PositionsCreate(CreateView):
    model = Positions
    form_class = positionsForm
    template_name = 'crear_position.html'
    success_url = reverse_lazy('index_position')

class PositionsUpdate(UpdateView):
    model = Positions
    form_class = positionsForm
    template_name = 'crear_position.html'
    success_url = reverse_lazy('index_position')   

class PositionsDelete(DeleteView):
    model = Positions
    template_name = 'verificacion.html'
    success_url = reverse_lazy('index_position') 

class FinalMenu(ListView):
    model = Menu
    template_name = 'final_menu.html'

    def get_queryset(self):
        return self.model.objects.all()