from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from .models import Menu, Positions
from .forms import menuForm
import json

# Create your views here.
def inicio(request):
    menus = Menu.objects.all()
    contexto = {
        'menus': menus
    }
    return render(request, 'index.html',contexto)

def crearMenu(request):
    if request.method == 'GET':
        form = menuForm()
        contexto = {
            'form': form
        }
    else:
        form = menuForm(request.POST)
        contexto = {
            'form': form
        }
        if form.is_valid():
            form.save()
            return redirect('index')
    return render(request, 'crear_menu.html',contexto)

def editarMenu(request, id):
    menu = Menu.objects.get(id=id)
    if request.method == 'GET':
        form = menuForm(instance = menu)
        contexto = {
            'form': form
        }
    else:
        form = menuForm(request.POST, instance = menu)
        contexto = {
            'form': form
        } 
        if form.is_valid():
            form.save()
            return redirect('index')
    return render(request, 'crear_menu.html', contexto)

def eliminarMenu(request, id):
    menu = Menu.objects.get(id=id)
    menu.delete()
    return redirect('index')

def Search(request):
    title = request.GET.get('title')
    menus = Menu.objects.filter(title__startswith=title)
    menus = [ menu_serializer(menu) for menu in menus]
    return HttpResponse(json.dumps(menus), content_type='application/json')

def menu_serializer(menu):
    return {'title': menu.title, 'description' : menu.description, 'link' : menu.link, 'status' : menu.status, 'position' : menu.position}