from django import forms
from .models import Menu, Positions

class menuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ('title', 'description', 'link', 'status', 'position')
        status = forms.BooleanField()
        position = forms.ModelChoiceField(queryset=Positions.objects.all())
        widgets = {
                    'title': forms.TextInput(attrs={'class': 'form-control'}),
                    'description': forms.Textarea(attrs={'class': 'form-control'}),
                    'link': forms.TextInput(attrs={'class': 'form-control'}),
                    }

        def __init__(self):
            self.fields['status'].widget.attrs.update({'class': 'form-check-input'})
            self.fields['position'].widget.attrs.update({'class': 'form-control'})
            if check_something():
                self.fields['status'].initial  = True

class positionsForm(forms.ModelForm):
    class Meta:
        model = Positions
        fields = ('name', 'description', 'status')
        status = forms.BooleanField()
        widgets = {
                    'name': forms.TextInput(attrs={'class': 'form-control'}),
                    'description': forms.Textarea(attrs={'class': 'form-control'}),
                    }

        def __init__(self):
            self.fields['status'].widget.attrs.update({'class': 'form-check-input'})
            if check_something():
                self.fields['status'].initial  = True


