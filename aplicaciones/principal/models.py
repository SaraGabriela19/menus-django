from django.db import models

# Create your models here.
class Positions(models.Model):
    id = models.AutoField(primary_key= True)
    name = models.CharField(max_length= 250)
    description = models.TextField()
    status = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)  

    def __str__(self):
        return self.name

class Menu(models.Model):
    id = models.AutoField(primary_key= True)
    title = models.CharField(max_length= 250)
    description = models.TextField()
    status = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    link = models.CharField(max_length= 255)
    position = models.ForeignKey(Positions, on_delete=models.CASCADE, null=True) 

    def __str__(self):
        return self.title
